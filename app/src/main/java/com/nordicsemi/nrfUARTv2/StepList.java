package com.nordicsemi.nrfUARTv2;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by aboveSphinx on 10/25/13.
 */
public class StepList {
    /**
     * Constants
     */
    public static final float METERS_PER_SEC_TO_MPH = 2.23694f;   // meter/sec to MPH
    private static final int RUNNING = 0;
    private static final int WALKING = 1;
    private static int STANDARD_FORM_BUFFER_SIZE = 180;
    private static int SMOOTH_BUFFER_SIZE = 4;

    /**
     * Data smoothing
     */
    private static float[] sSmoothStrideTimeBuffer = new float[SMOOTH_BUFFER_SIZE];
    private static float[] sSmoothSpeedBuffer = new float[SMOOTH_BUFFER_SIZE];
    private static float[] sSmoothEfficiencyBuffer = new float[SMOOTH_BUFFER_SIZE];
    private static float[] sSmoothImpactAngleBuffer = new float[SMOOTH_BUFFER_SIZE];
    private static float[] sSmoothUpImpactBuffer = new float[SMOOTH_BUFFER_SIZE];
    private static float[] sSmoothGroundTimeBuffer = new float[SMOOTH_BUFFER_SIZE];

    private static float[] sSmoothStrideLengthBuffer = new float[SMOOTH_BUFFER_SIZE];
    private static float[] sSmoothContactRatioBuffer = new float[SMOOTH_BUFFER_SIZE];
    private static float[] sSmoothBackKickAngleBuffer = new float[SMOOTH_BUFFER_SIZE];

    private static int sSmoothBuffFillPtr = 0;

    // Real time values (After smoothing)
    private static float sRtStrideTime = 0;
    private static float sRtGroundTime = 0;
    public static float sRunDuration = 0;
    public static float sWalkDuration = 0;

    public static float sRtSpm = 0;
    public static float sRtGpsSpeed = 0;
    public static float sRtDeviceSpeed = 0;
    public static float sRtGpsDistance = 0;
    public static float sRtDeviceDistance = 0;
    public static float sRtCalories = 0;
    public static float sRtEfficiency = 0;
    public static float sRtImpactAngle = 0;
    public static float sRtContactRatio = 0;
    public static float sRtStrideLength = 0;
    public static float sRtBackKickAngle = 0;

    // Used for main UI
    public static ArrayList<Float> sRtSpmList = new ArrayList<Float>();
    public static ArrayList<Float> sRtGpsSpeedList = new ArrayList<Float>();
    public static ArrayList<Float> sRtDeviceSpeedList = new ArrayList<Float>();
    public static ArrayList<Float> sRtGpsDistanceList = new ArrayList<Float>();
    public static ArrayList<Float> sRtDeviceDistanceList = new ArrayList<Float>();
    public static ArrayList<Float> sRtCaloriesList = new ArrayList<Float>();
    public static ArrayList<Float> sRtEfficiencyList = new ArrayList<Float>();
    public static ArrayList<Float> sRtImpactAngleList = new ArrayList<Float>();
    public static ArrayList<Float> sRtStrideLengthList = new ArrayList<Float>();
    public static ArrayList<Float> sRtContactRatioList = new ArrayList<Float>();
    public static ArrayList<Float> sRtBackKickAngleList = new ArrayList<Float>();
    public static ArrayList<Long> sRtTimestampList = new ArrayList<Long>();

    // Used for video analysis UI
    public static ArrayList<Float> sRtStrideTimeList = new ArrayList<Float>();
    public static ArrayList<Float> sRtGroundTimeList = new ArrayList<Float>();

    // Used for fatigue
    public static ArrayList<Float> sRtStrideTimeDiffList = new ArrayList<Float>();
    public static ArrayList<Float> sRtGroundTimeDiffList = new ArrayList<Float>();
    public static ArrayList<Float> sRtStrideLengthDiffList = new ArrayList<Float>();

    /**
     * For interaction
     */
    public static int sRunSteps = 0;
    public static int sWalkSteps = 0;

    // Pointers to the stored stride data which is currently being used for real time coaching
    public static int sLastUpdateIndex = 0;
    public static int sLastInstructionIndex = 0;
    public static float sLastImpact = 0;
    public static float sLastEfficiency = 0;

    // Initial activity recognition is null
    public static int sActivity = -1;

    // Filtering used to throw out bad data
    private static int INCOMING_BUFFER_SIZE = 2;
    private static float[] sIncomingBuffer = new float[INCOMING_BUFFER_SIZE];
    private static int sIncomingFillptr = 0;
    private static float sValidIncomingScore = 200;

    /**
     * Flags for user update preferences
     */
    public static boolean sSpeakDistance = true;
    public static boolean sSpeakCadence = true;
    public static boolean sSpeakMileTime = false;
    public static boolean sSpeakContact = false;
    public static boolean sSpeakImpact = false;
    public static boolean sSpeakEfficiency = false;
    public static boolean sCadenceAssistance = false;
    public static boolean sImprovementMessage = false;
    public static boolean sCoachingMessage = false;

    public static int sIdealCadence = 180;
    public static int sGoodSteps = 0;
    public static int sBadSteps = 0;

    public static float sEfficiencyBadGoodThresh = 75;
    public static float sImpactBadGoodThresh = 75;
    public static float sEfficiencyVeryGoodThresh = 85;
    private static float sWalkRunThresh = 800; //ms
    private static float sWalkRunGroundThresh = 500; //ms
    private static boolean sStartRecording = false;
    public static boolean mCountAllSteps = false;
    private static ArrayList<Double> sProximityLog = new ArrayList<Double>();
    private static int sUpdateCnt = 0;

    /**
     * For coaching
     */
    private static boolean sIsInitialRunning = true;

    public static ArrayList<Float> sTrueRunCadence = new ArrayList<Float>();
    public static ArrayList<Float> sTrueRunSpeed = new ArrayList<Float>();
    public static ArrayList<Float> sTrueRunBackkickAngle = new ArrayList<Float>();
    public static ArrayList<Float> sTrueRunImpactAngle = new ArrayList<Float>();

    public static float sStartRunningCadence = 0;
    public static float sStartRunningSpeed = 0;
    public static float sStartRunningBackkickAngle = 0;
    public static float sStartRunningImpactAngle = 0;

    private static int sFormFillPtr = 0;
    public static double[] sCurrentRunningFormMean = new double[6];
    public static double[] sStartRunningFormMean = new double[6];
    private static double[][] sRecentFormData = new double[6][STANDARD_FORM_BUFFER_SIZE];

    /**
     * Used to append new stride information to lists
     */
    public static void runListAppend(final int runCount, final int walkCount,
                                     final float stride_time_ms, final float ground_time_ms,
                                     final float stride_length_cm,  final float back_kick_angle,
                                     final float impact_angle, final float up_imp,
                                     final float gps_speed, final float gps_distance,
                                     final float device_distance) {
        if (sStartRecording) {
            sIncomingFillptr++;
            sIncomingFillptr = sIncomingFillptr % INCOMING_BUFFER_SIZE;
            sIncomingBuffer[sIncomingFillptr] = stride_time_ms;

            String metaString = String.format("%d", sRunSteps) +
                    "," + String.format("%.0f", sRunDuration) +
                    "," + String.format("%.0f", sRtCalories) +
                    "," + String.format("%.0f", sRtDeviceDistance);

            DataLogger.recordMetadata(metaString);

            float score = Util.absDiffSum(sIncomingBuffer);
            // Ignore clearly non step data
            if (!(score < sValidIncomingScore && stride_time_ms > 400 && stride_time_ms < 2000 &&
                    ground_time_ms > 75 && ground_time_ms < 1500 && back_kick_angle < 1000)) {
                return;
            }

            // Airtime can be calculated from stride_time and ground_time
            float air_time = (stride_time_ms - 2.0f * ground_time_ms);
            if (air_time < 0)
                air_time = 0;

            float efficiency = Util.normalizeEfficiencyGnd(stride_time_ms, ground_time_ms);

            float device_speed = (float) ((stride_length_cm / 100.0) / (stride_time_ms / 1000.0)); // meters per sec
            // Real time displayed data has smoothing
            sSmoothBuffFillPtr++;
            sSmoothBuffFillPtr %= SMOOTH_BUFFER_SIZE;

            sSmoothStrideTimeBuffer[sSmoothBuffFillPtr] = stride_time_ms; // in milisec
            sSmoothEfficiencyBuffer[sSmoothBuffFillPtr] = efficiency; // in percent
            sSmoothImpactAngleBuffer[sSmoothBuffFillPtr] = impact_angle; // in degrees
            sSmoothGroundTimeBuffer[sSmoothBuffFillPtr] = ground_time_ms; // in milisec
            sSmoothContactRatioBuffer[sSmoothBuffFillPtr] = 100 * ground_time_ms / stride_time_ms; // in percent
            sSmoothBackKickAngleBuffer[sSmoothBuffFillPtr] = back_kick_angle; // in degrees
            sSmoothStrideLengthBuffer[sSmoothBuffFillPtr] = stride_length_cm; // in centimeters
            sSmoothSpeedBuffer[sSmoothBuffFillPtr] = device_speed; // in meters/sec

            sRtStrideTime = Util.mean(sSmoothStrideTimeBuffer);
            sRtStrideTimeList.add(sRtStrideTime);
            sRtSpm = (1000.0f / sRtStrideTime) * 60.0f * 2.0f;
            sRtSpmList.add(sRtSpm);
            sRtGpsSpeed = gps_speed;
            sRtGpsSpeedList.add(sRtGpsSpeed);
            sRtDeviceSpeed = Util.mean(sSmoothSpeedBuffer);
            sRtDeviceSpeedList.add(sRtDeviceSpeed);
            sRtGpsDistance = gps_distance;
            sRtGpsDistanceList.add(sRtGpsDistance);
            sRtDeviceDistance = device_distance;
            sRtDeviceDistanceList.add(sRtDeviceDistance);
            sRtCalories = getCalories();
            sRtCaloriesList.add(sRtCalories);
            sRtEfficiency = Util.mean(sSmoothEfficiencyBuffer);
            sRtEfficiencyList.add(sRtEfficiency);
            sRtImpactAngle = Util.mean(sSmoothImpactAngleBuffer);
            sRtImpactAngleList.add(sRtImpactAngle);
            sRtGroundTime = Util.mean(sSmoothGroundTimeBuffer);
            sRtGroundTimeList.add(sRtGroundTime);
            sRtContactRatio = Util.mean(sSmoothContactRatioBuffer);
            sRtContactRatioList.add(sRtContactRatio);
            sRtBackKickAngle = Util.mean(sSmoothBackKickAngleBuffer);
            sRtBackKickAngleList.add(sRtBackKickAngle);
            sRtStrideLength = Util.mean(sSmoothStrideLengthBuffer);
            sRtStrideLengthList.add(sRtStrideLength);
            sRtTimestampList.add(System.currentTimeMillis());

            // Activity recognition from stride time ( st < 850 ms) and ground time (gt < 500 ms)
            if (sRtStrideTime <= sWalkRunThresh && sRtGroundTime < sWalkRunGroundThresh && true) {
                sActivity = RUNNING;

                // Prepare information for coaching
//                    sTrueRunCadence.add(sRtSpm);
//                    sTrueRunBackkickAngle.add(sRtBackKickAngle);
//                    sTrueRunImpactAngle.add(sRtImpactAngle);
//                    sTrueRunSpeed.add(sRtDeviceSpeed);

                sRunDuration = sRunDuration + (stride_time_ms / 1000f);
                sRunSteps++;

                // If running start tracking changes in the following metrics
                double[] data = {stride_time_ms, ground_time_ms, back_kick_angle, air_time,
                        stride_length_cm, impact_angle};
                for (int k = 0; k < 6; k++) {
                    sRecentFormData[k][sFormFillPtr] = data[k];
                }

                // Fatigue info / run change (is zero for walking and first two minutes)
                sFormFillPtr++;
                if (sFormFillPtr == STANDARD_FORM_BUFFER_SIZE) {
                    sIsInitialRunning = false;
                    sFormFillPtr = 0;
                }
                float st_diff;
                float gt_diff;
                float sl_diff;

                // Find change in all 6 metrics from start of run (or define start of run)
                for (int k = 0; k < 6; k++) {
                    double[] ndm = sRecentFormData[k];
                    double u = Util.mean(ndm);
                    double sig = Util.std(ndm);
                    ArrayList<Double> noOutliers = new ArrayList<Double>();
                    for (int i = 0; i < STANDARD_FORM_BUFFER_SIZE; i++) {
                        if (ndm[i] > u - 1 * sig && ndm[i] < u + 1 * sig) {
                            noOutliers.add(ndm[i]);
                        }
                    }
                    if (sIsInitialRunning) {
                        sStartRunningFormMean[k] = Util.meanD(noOutliers);
                    } else {
                        sCurrentRunningFormMean[k] = Util.meanD(noOutliers);
                    }
                }
                // After initial run form was collected on first 180 strides track the change in metrics
                if (!sIsInitialRunning) {
                    st_diff = (float) (100 * (sCurrentRunningFormMean[0] -
                            sStartRunningFormMean[0]) / sStartRunningFormMean[0]);
                    gt_diff = (float) (100 * (sCurrentRunningFormMean[1] -
                            sStartRunningFormMean[1]) / sStartRunningFormMean[1]);
                    sl_diff = (float) (100 * (sCurrentRunningFormMean[4] -
                            sStartRunningFormMean[4]) / sStartRunningFormMean[4]);
                    sRtStrideTimeDiffList.add(st_diff);
                    sRtGroundTimeDiffList.add(gt_diff);
                    sRtStrideLengthDiffList.add(sl_diff);
                } else {
                    sRtStrideTimeDiffList.add(0f);
                    sRtGroundTimeDiffList.add(0f);
                    sRtStrideLengthDiffList.add(0f);
                }
            } else {
                sActivity = WALKING;
                sWalkDuration = sWalkDuration + (stride_time_ms / 1000f); // stride time in ms
                sWalkSteps++;

                sRtStrideTimeDiffList.add(0f);
                sRtGroundTimeDiffList.add(0f);
                sRtStrideLengthDiffList.add(0f);
            }

            DataLogger.recordDeviceResult(runCount, walkCount, stride_time_ms, ground_time_ms,
                    stride_length_cm, back_kick_angle, impact_angle, up_imp, sRtCalories,
                    getTotalFatigue());
        }
    }

    /* init()
    Used to create a new data log for a new running session, saves the old session data to a list
    */
    public static void init() {
        // Reset Step counters
        sWalkSteps = 0;
        sRunSteps = 0;
        sRunDuration = 0;
        sWalkDuration = 0;

        // Clear differential measures
        sLastEfficiency = 0;
        sLastImpact = 0;
        sLastUpdateIndex = 0;

        // Reset activity recognition
        sActivity = -1;

        // Clear real time lists
        sRtSpmList.clear();
        sRtGpsSpeedList.clear();
        sRtDeviceSpeedList.clear();
        sRtDeviceDistanceList.clear();
        sRtCaloriesList.clear();
        sRtGpsDistanceList.clear();
        sRtEfficiencyList.clear();
        sRtImpactAngleList.clear();
        sRtTimestampList.clear();
        sRtContactRatioList.clear();
        sRtBackKickAngleList.clear();
        sRtStrideLengthList.clear();
        sRtStrideTimeList.clear();
        sRtGroundTimeList.clear();
        sRtGroundTimeDiffList.clear();
        sRtStrideLengthDiffList.clear();
        sRtStrideTimeDiffList.clear();

        //Clear real time buffers
//        Arrays.fill(sSmoothStrideTimeBuffer, 0);
        // If all stride times start at zero, initial stride frequency is infinite
        // Initialize initial stride frequency to 120 SPM (medium speed walking) instead of inf
        for (int i = 0; i < sSmoothStrideTimeBuffer.length; i++)
            sSmoothStrideTimeBuffer[i] = 1000.0f;

        Arrays.fill(sSmoothGroundTimeBuffer, 0);
        Arrays.fill(sSmoothSpeedBuffer, 0);
        Arrays.fill(sSmoothEfficiencyBuffer, 0);
        Arrays.fill(sSmoothImpactAngleBuffer, 0);
        Arrays.fill(sSmoothUpImpactBuffer, 0);
        Arrays.fill(sSmoothContactRatioBuffer, 0);
        Arrays.fill(sSmoothBackKickAngleBuffer, 0);
        Arrays.fill(sSmoothStrideLengthBuffer, 0);
        sSmoothBuffFillPtr = 0;

        // Reset coach message flags
        sImprovementMessage = false;
        sCoachingMessage = false;
        sGoodSteps = 0;
        sBadSteps = 0;

        approaching = false;
        sStartRecording = false;


        last_proximity = 0;
        moving_away = 0;
        moving_towards = 0;
        approaching = false;
        global_min = 1e6;
        minp = 0;
        location_passed = false;
        sUpdateCnt = 0;

        Log.d("STEP", "Values reset");
    }

    public static void start(int mode) {
        //mode = 0 -> data with no video
        //mode = 1 -> data with video
        sStartRecording = true;

        // Get timestamp of first connect (for video alignment)
        if (mode == 1) {
            sRtSpmList.add(0f);
            sRtGpsSpeedList.add(0f);
            sRtEfficiencyList.add(0f);
            sRtImpactAngleList.add(0f);
            sRtBackKickAngleList.add(0f);
            sRtStrideLengthList.add(0f);
            sRtContactRatioList.add(0f);

            sRtTimestampList.add(System.currentTimeMillis());
        }
    }

    public static double last_proximity = 0;
    public static double moving_away = 0;
    public static double moving_towards = 0;
    public static boolean approaching = false;
    public static double global_min = 1e6;
    public static double minp = 0;
    public static boolean location_passed = false;

    public static boolean simpleGeoMarker(double lat, double lng, double targetLat,
                                          double targetLng) {
        double proximity = Math.sqrt((lat - targetLat) * (lat - targetLat)
                + (lng - targetLng) * (lng - targetLng));
        sUpdateCnt++;

        if (proximity < last_proximity) {  // falling
            moving_away = 0;
            moving_towards = moving_towards + 1;
            if (moving_towards > 10) {
                approaching = true;
            }
        } else if (proximity > last_proximity) { //rising
            moving_away = moving_away + 1;
            moving_towards = 0;
            if (approaching) {
                if (proximity < global_min) {
                    global_min = proximity;
                    location_passed = true;
                }

                double NOISE_FLOOR = 0.003;
                if (moving_away > 3 && location_passed &&
                        global_min < NOISE_FLOOR && sUpdateCnt > 10) {
                    location_passed = false;
                    return true;
                }
            }
        }

        last_proximity = proximity;
        sProximityLog.add(proximity);
        return false;
    }

    public static void addSessionEnding() {
        for (int i = 0; i < 5; i++) {
            sRtSpmList.add(0f);
            sRtDeviceSpeedList.add(0f);
            sRtGpsSpeedList.add(0f);
            sRtCaloriesList.add(sRtCalories);
            sRtDeviceDistanceList.add(sRtDeviceDistance);
            sRtGpsDistanceList.add(sRtGpsDistance);
            sRtEfficiencyList.add(0f);
            sRtImpactAngleList.add(0f);
            sRtBackKickAngleList.add(0f);
            sRtContactRatioList.add(0f);
            sRtStrideLengthList.add(0f);
            sRtTimestampList.add(System.currentTimeMillis());
        }
    }

    public static float getCalories() {
        float calories = 0;
        for (int i = 0; i < sRtDeviceSpeedList.size(); i++) {
            float v = sRtDeviceSpeedList.get(i);  // stride speed (meters per sec)
            float t = 120.0f / sRtSpmList.get(i);  // stride time (secs)
            if (v > 0 && t > 0 && t < 3 && v <= 15) {  // humanly possible
                if (v >= 5 && v <= 15)
                    calories += (116.4f * v + 71.78f) * t / 3600.0f;
                else if (v >= 0)
                    calories += (14f * v * v - 51.4f * v + 286.2f) * t / 3600.0f;
            }
        }
        return calories * 2;
    }

    public static float getAveragePace(int startIndex) {
        float avgSped = Util.mean(sRtDeviceSpeedList.subList(startIndex,
                sRtDeviceSpeedList.size()));
        return Util.speedToPace(avgSped);
    }

    public static float getAverageCadence(int startIndex) {
        return Util.mean(sRtSpmList.subList(startIndex, sRtSpmList.size()));
    }

    public static float getFatigue(int index) {
        return Math.abs(StepList.sRtStrideTimeDiffList.get(index)) +
                Math.abs(StepList.sRtGroundTimeDiffList.get(index)) +
                Math.abs(StepList.sRtStrideLengthDiffList.get(index));
    }

    public static float getTotalFatigue() {
        int fatigue = 0;
        if (sRtStrideTimeDiffList.size() > 0) {
            int index = sRtStrideTimeDiffList.size() - 1;
            fatigue = (int) (Math.abs(StepList.sRtStrideTimeDiffList.get(index)) +
                    Math.abs(StepList.sRtGroundTimeDiffList.get(index)) +
                    Math.abs(StepList.sRtStrideLengthDiffList.get(index)));
        }
        return fatigue;
    }

    public static void setSmooth(int window) {
        // Data smooth buffer
        SMOOTH_BUFFER_SIZE = window;
        sSmoothStrideTimeBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothSpeedBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothEfficiencyBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothImpactAngleBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothUpImpactBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothGroundTimeBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothStrideLengthBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothContactRatioBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothBackKickAngleBuffer = new float[SMOOTH_BUFFER_SIZE];
        sSmoothBuffFillPtr = 0;
    }

    public static void clearBuffer() {
        sRtStrideTime = 0;
        sRtGpsSpeed = 0;
        sRtEfficiency = 0;
        sRtImpactAngle = 0;
        sRtDeviceSpeed = 0;
        for (int i = 0; i < SMOOTH_BUFFER_SIZE; i++) {
            sSmoothStrideTimeBuffer[i] = 2000.0f;
            sSmoothGroundTimeBuffer[i] = 0.0f;
            sSmoothSpeedBuffer[i] = 0.0f;
            sSmoothEfficiencyBuffer[i] = 0.0f;
            sSmoothUpImpactBuffer[i] = 0.0f;
            sSmoothImpactAngleBuffer[i] = 0.0f;
            sSmoothStrideLengthBuffer[i] = 0;
            sSmoothContactRatioBuffer[i] = 0;
            sSmoothBackKickAngleBuffer[i] = 0;
        }
    }
}