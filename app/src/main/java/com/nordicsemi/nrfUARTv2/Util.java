package com.nordicsemi.nrfUARTv2;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cnbuff410 on 3/5/14.
 */
public class Util {

    private static final float GOLDEN_CADENCE = 180;
    private static final float GOLDEN_AIRTIME = 40;
    private static final float GOLDEN_IMPACT = 10;

    /**
     * Calculate the target color based on progress
     *
     * @param progress: normalized into 0 - 1
     * @param Colors:   Color arrays
     * @return Target color
     */
    public static int calculateColor(float progress, int[] Colors) {
        float unit = progress;
        if (unit < 0) {
            unit += 1;
        }

        if (unit <= 0) {
            return Colors[0];
        }
        if (unit >= 1) {
            return Colors[Colors.length - 1];
        }

        float p = unit;

        int c0 = Colors[0];
        int c1 = Colors[Colors.length - 1];
        int a = ave(Color.alpha(c0), Color.alpha(c1), p);
        int r = ave(Color.red(c0), Color.red(c1), p);
        int g = ave(Color.green(c0), Color.green(c1), p);
        int b = ave(Color.blue(c0), Color.blue(c1), p);

        return Color.argb(a, r, g, b);
    }

    //TODO update this code, its stack overflow real old sample... not too accurate)
    /**
     * float meters = gpsDist()
     * Returns the distance (in meters) between two successive gps lat/long data point pairs
     */
    public static float gpsDist(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        int meterConversion = 1609;
        float dist_m = new Float(dist * meterConversion).floatValue();
        return dist_m;
    }

    public static float normalizeCadence(float cadence) {
        return 100 - Math.abs(cadence - GOLDEN_CADENCE);
    }

    public static float normalizeEfficiency(float airtime) {
        float effPercent = 100f - (airtime - GOLDEN_AIRTIME) / 2f;
        effPercent = effPercent > 100 ? 100 : effPercent;
        effPercent = effPercent < 0 ? 0 : effPercent;
        return effPercent;
    }

    public static float normalizeEfficiencyGnd(float strideTime, float groundTime) {
//        float effPercent = 100f + 80f * (1f - strideTime / (2f * groundTime));
        float airtime = 0.5f * (strideTime - 2f * groundTime);
        float effPercent = 200f - 100f * strideTime / 600f - (airtime * airtime) / 2000f;
        effPercent = effPercent > 100 ? 100 : effPercent;
        effPercent = effPercent < 0 ? 0 : effPercent;
        return effPercent;
    }

    public static float normalizeSafety(float impact) {
        float safetyPercent = 100f * (1f - (impact - 100f) / 400f);
        safetyPercent = safetyPercent > 100 ? 100 : safetyPercent;
        safetyPercent = safetyPercent < 0 ? 0 : safetyPercent;
        return safetyPercent;
    }

    // Use form instead of safety?
    public static float normalizeForm(float stride_length, float stride_time, float back_kick) {
        float ideal_stride_time = 666; // 180 cadence
        float formPercent = (1f - Math.abs(stride_time - ideal_stride_time) / 400f);
        // 80 % determined by cadence
        formPercent = 80 * formPercent;
        // up to 20 % bonus for more back kick and stride lengths (more speed at cadence goal)
        formPercent = formPercent + stride_length / 40;
        formPercent = formPercent + back_kick / 10;
        formPercent = formPercent > 100 ? 100 : formPercent;
        formPercent = formPercent < 0 ? 0 : formPercent;
        return formPercent;
    }

    /*
     Basic statistical calculation functions
     */
    public static int ave(int s, int d, float p) {
        return s + Math.round(p * (d - s));
    }

    public static float mean(float[] data) {
        float mean = 0.0f;
        for (float v : data) {
            mean += v;
        }
        mean /= data.length;
        return mean;
    }

    public static float mean(List<Float> data) {
        float mean = 0.0f;
        for (float v : data) {
            mean += v;
        }
        mean /= data.size();
        return mean;
    }

    public static double meanD(ArrayList<Double> data) {
        double mean = 0.0f;
        for (double v : data) {
            mean += v;
        }
        mean /= data.size();
        return mean;
    }

    public static double mean(double[] data) {
        double mean = 0.0f;
        for (double v : data) {
            mean += v;
        }
        mean /= data.length;
        return mean;
    }

    public static float std(float[] data) {
        float n = 0;
        float mean = 0;
        float M2 = 0;

        for (int i = 0; i < data.length; i++) {
            n = n + 1;
            float delta = data[i] - mean;
            mean = mean + delta / n;
            M2 = M2 + delta * (data[i] - mean);
        }

        return (float) Math.sqrt(M2 / (n - 1));
    }

    public static float std(ArrayList<Float> data) {
        float n = 0;
        float mean = 0;
        float M2 = 0;

        for (int i = 0; i < data.size(); i++) {
            n = n + 1;
            float delta = data.get(i) - mean;
            mean = mean + delta / n;
            M2 = M2 + delta * (data.get(i) - mean);
        }

        return (float) Math.sqrt(M2 / (n - 1));
    }


    public static float sum(float[] data) {
        float sum = 0.0f;
        for (float v : data) {
            sum += v;
        }
        return sum;
    }

    public static float sum(ArrayList<Float> data) {
        float sum = 0.0f;
        for (float v : data) {
            sum += v;
        }
        return sum;
    }

    public static double sum(double[] data) {
        double sum = 0.0f;
        for (double v : data) {
            sum += v;
        }
        return sum;
    }

    public static double std(double[] data) {
        double sum = 0.0f;
        for (double v : data) {
            sum += v;
        }
        double mean = sum / data.length;
        double var = 0;
        for (double v : data) {
            var += (v - mean) * (v - mean);
        }
        return Math.sqrt(var);
    }

    public static float max(float[] data) {
        float max = Float.MIN_VALUE;
        for (float v : data) {
            if (v > max)
                max = v;
        }
        return max;
    }

    public static float max(ArrayList<Float> data) {
        float max = Float.MIN_VALUE;
        for (float v : data) {
            if (v > max)
                max = v;
        }
        return max;
    }

    public static float min(float[] data) {
        float min = Float.MAX_VALUE;
        for (float v : data) {
            if (v < min)
                min = v;
        }
        return min;
    }

    public static float min(ArrayList<Float> data) {
        float min = Float.MAX_VALUE;
        for (float v : data) {
            if (v < min)
                min = v;
        }
        return min;
    }

    public static float absDiffSum(float[] data) {
        float diff = 0;
        for (int i = 0; i < data.length - 1; i++) {
            diff += Math.abs(data[i] - data[i + 1]);
        }
        return diff;
    }

    /**
     * Convert speed(meter/sec) to pace(min/mile)
     * @param speed
     * @return
     */
    public static float speedToPace(float speed) {
        return 1.0f / speed / StepList.METERS_PER_SEC_TO_MPH * 60;
    }

    public static ArrayList<Float> reject(ArrayList<Float> data) {
        float lastVal = 0;
        ArrayList<Float> sTempData = new ArrayList<Float>();
        // Reject outliers
        for (int i = 0; i < data.size(); i++) {

            if (data.get(i) == 0) {
                sTempData.add(lastVal);
            } else {
                sTempData.add(data.get(i));
                lastVal = data.get(i);
            }
        }
        return sTempData;
    }

    public static ArrayList<Float> smooth(ArrayList<Float> data, int wind) {
        if (wind == 0) {
            return data;
        }
        ArrayList<Float> data_t = new ArrayList<Float>();
        // Smooth the data
        for (int i = 0; i < data.size(); i++) {
            float meanCherry = 0;
            float countCherry = 0;
            for (int j = 0; j < wind; j++) {
                int ind = i + j;
                if (ind > data.size() - 1) {
                    ind = data.size() - 1;
                }
                meanCherry += data.get(ind);
                countCherry++;
            }
            data_t.add(meanCherry / countCherry);
        }
        return data_t;
    }

    public static boolean stringToBool(String s) {
        if (s.equals("1"))
            return true;
        if (s.equals("0"))
            return false;
        throw new IllegalArgumentException(s+" is not a bool. Only 1 and 0 are.");
    }
}
