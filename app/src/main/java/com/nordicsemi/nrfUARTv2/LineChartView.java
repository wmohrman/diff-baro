package com.nordicsemi.nrfUARTv2;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * A template line chart view with multi-touch interaction supported.
 * Only provide absolutely necessary basic functions.
 * Used as a template for other line chart views.
 */
public class LineChartView extends View implements GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {
    /**
     * Constants
     */
    private final float DENSITY = getContext().getResources()
            .getDisplayMetrics().density;
    private static final int SWIPE_MIN_DISTANCE = 100;
    private static final int SWIPE_THRESHOLD_VELOCITY = 100;
    private static final int INNER_MARK_RADIUS = 10;
    private static final int OUTER_MARK_RADIUS = 16;

    /**
     * Paints
     */
    private Paint mSignalPaint;
    private Paint mInnerMarkPaint;
    private Paint mOuterMarkPaint;
    private Paint mFlagPaint;
    private Paint mAxesPaint;
    private Paint mAxesMarkPaint;
    private Paint mTicsPaint;
    private Paint mCirclePaint;
    private Paint mTextPaint;
    private Paint mTitlePaint;

    /**
     * Title
     */
    private String mTitle = "";
    private float mTitleHeight = 25 * DENSITY;
    private float mTitleMarginBottom = 6 * DENSITY;
    private String mText = "";

    /**
     * Axes plot
     */
    private float mXMarksSpace = 10 * DENSITY;
    private float mYMarksSpace = 15 * DENSITY;
    private float mTitleSpace = 15 * DENSITY;
    private float mAxesLeft;
    private float mAxesRight;
    private float mAxesTop;
    private float mAxesBottom;

    /**
     * YTics
     */
    private float mYTicsMax;
    private float mYTicsMin;
    private float mYTicsHeight = 5 * DENSITY;
    private String mYticsUnit = "";
    private int mYTicsNum = 4;

    /**
     * Other Drawing Vars
     */
    private ArrayList<float[]> mDataXList;
    private ArrayList<float[]> mDataYList;
    private ArrayList<Path> mSignalPathList;
    private int[] mColors = new int[]{
            getResources().getColor(android.R.color.holo_red_dark),
            getResources().getColor(android.R.color.holo_green_light),
            getResources().getColor(android.R.color.holo_purple),
            getResources().getColor(android.R.color.holo_blue_dark),
            getResources().getColor(android.R.color.holo_blue_bright),
            getResources().getColor(android.R.color.holo_red_light)
    };
    private int mSize = 0;
    private int mLineNum = 0;
    private int mHeight, mWidth;
    private boolean mDrawRealTime = true;

    private GestureDetector mGestureDetector;

    public static double mScaleFactor = 1.0;
    private float mOffset = 0;
    private int mStart = 0;
    private int mWindowSize = 1000;
    private int mStop = mWindowSize;
    private float mBackgroundRtScrollPosition = 0;
    private static int sScrollByX = 0;
    public static float sDownX = 0;
    public static float sTotalX = 0;
    public static float sMaxSize = 0;

    public static boolean sPlotAreaChart = false;
    public static boolean sBlackChartTitle = false;

    private float[] mTrueCircleVal = new float[2];
    private boolean mFirstTouch = false;
    private boolean mIsPlotInit = false;
    private long mLastTime;

    public boolean isInit() {
        return mIsPlotInit;
    }

    // all the listener stuff below
    public interface MyDoubleClickListener {
        public void onStateChange(boolean state);
    }

    private MyDoubleClickListener myDoubleClickListener = null;

    public void registerListener(MyDoubleClickListener listener) {
        myDoubleClickListener = listener;
    }

    /* Empty listener is used to avoid null exception */
    OnViewInteractionListener mListener = new OnViewInteractionListener() {
        @Override
        public void onMove(float totalX) {
        }
    };

    public static interface OnViewInteractionListener {
        public void onMove(float totalX);
    }

    public static void setMax(float max) {
        sMaxSize = max;
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        // Let the ScaleGestureDetector inspect all events.

        if (motionEvent.getPointerCount() < 2) {
            float currentX;
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    sDownX = motionEvent.getX();
                    sTotalX = mBackgroundRtScrollPosition;

                    // Custom double click detector
                    long thisTime = System.currentTimeMillis();
                    if (mFirstTouch && (thisTime - mLastTime) <= 300) {
                        if (myDoubleClickListener != null) {
                            myDoubleClickListener.onStateChange(true);
                        }
                        Log.d("Wyatt1", "double click here");
                        mFirstTouch = false;
                    } else {
                        mFirstTouch = true;
                    }
                    mLastTime = thisTime;
                    break;

                case MotionEvent.ACTION_MOVE:
                    currentX = motionEvent.getX();
                    sScrollByX = (int) (sDownX - currentX);
                    float TOUCH_SENSITIVITY = 0.15f; // scalar to adjust scrolling sensitivity
                    sTotalX += sScrollByX * TOUCH_SENSITIVITY;
                    mListener.onMove(sTotalX);
                    sDownX = currentX;
                    break;

                case MotionEvent.ACTION_UP:
                    mBackgroundRtScrollPosition = sTotalX;
                    break;
            }
        }

        return true;
    }

    public LineChartView(Context context) {
        super(context);
        initGraph();
    }

    public LineChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.LineChartView,
                0, 0);
        mLineNum = a.getInt(R.styleable.LineChartView_lineNumber, 0);
        initGraph();
    }

    private void initGraph() {
        mSignalPaint = new Paint();
        mSignalPaint.setColor(getResources().getColor(android.R.color.holo_blue_light));
        mSignalPaint.setStrokeWidth(10);
        mSignalPaint.setStyle(Paint.Style.STROKE);

        mFlagPaint = new Paint();

        mInnerMarkPaint = new Paint();
        mInnerMarkPaint.setColor(getResources().getColor(android.R.color.white));
        mInnerMarkPaint.setStyle(Paint.Style.FILL);

        mOuterMarkPaint = new Paint();
        mOuterMarkPaint.setColor(getResources().getColor(android.R.color.holo_blue_light));
        mOuterMarkPaint.setStrokeWidth(OUTER_MARK_RADIUS - INNER_MARK_RADIUS);
        mOuterMarkPaint.setStyle(Paint.Style.STROKE);

        mAxesPaint = new Paint();
        mAxesPaint.setColor(getResources().getColor(android.R.color.darker_gray));
        mAxesPaint.setStrokeWidth(2);
        mAxesPaint.setStyle(Paint.Style.STROKE);

        mAxesMarkPaint = new Paint();
        mAxesMarkPaint.setColor(getResources().getColor(android.R.color.darker_gray));
        mAxesMarkPaint.setStrokeWidth(2);
        mAxesMarkPaint.setStyle(Paint.Style.STROKE);

        mCirclePaint = new Paint();
        String familyName = "";

        Typeface font = Typeface.create(familyName, Typeface.ITALIC);
//        Typeface fontNotation = Typeface.createFromAsset(getContext().getAssets(),
//                "RBNo2-Light-title.otf");

        mCirclePaint.setTypeface(font);
        mCirclePaint.setTextAlign(Align.LEFT);
        mCirclePaint.setTextSize(50);
        mCirclePaint.setColor(getResources().getColor(android.R.color.holo_blue_dark));
//        mCirclePaint.setTypeface(fontNotation);

        mTicsPaint = new Paint();

        mTicsPaint.setTypeface(font);
        mTicsPaint.setTextAlign(Align.LEFT);
        mTicsPaint.setTextSize(40);
        mTicsPaint.setColor(getResources().getColor(android.R.color.holo_blue_dark));
//        mTicsPaint.setTypeface(fontNotation);

        mTextPaint = new Paint();
        mTextPaint.setTypeface(font);
        mTextPaint.setTextAlign(Align.LEFT);
        mTextPaint.setTextSize(40);
        mTextPaint.setColor(getResources().getColor(android.R.color.holo_blue_dark));
//        mTextPaint.setTypeface(fontNotation);

        mTitlePaint = new Paint();
        mTitlePaint.setTextAlign(Align.CENTER);
        mTitlePaint.setTextSize(100);
        mTitlePaint.setColor(getResources().getColor(android.R.color.white));
//        mTitlePaint.setTypeface(fontNotation);

        mDataXList = new ArrayList<float[]>(mLineNum);
        mDataYList = new ArrayList<float[]>(mLineNum);
        mSignalPathList = new ArrayList<Path>(mLineNum);
        for (int i = 0; i < mLineNum; i++) {
            mDataXList.add(null);
            mDataYList.add(null);
            mSignalPathList.add(null);
        }

        mTrueCircleVal = new float[mLineNum];
        mIsPlotInit = true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mHeight = MeasureSpec.getSize(heightMeasureSpec);
        mWidth = MeasureSpec.getSize(widthMeasureSpec);
        mAxesLeft = mYMarksSpace;
        mAxesRight = mWidth;
        mAxesTop = mTitleSpace + mYTicsHeight;
        mAxesBottom = mHeight - mXMarksSpace;

        setMeasuredDimension(mWidth, mHeight);
    }

    public void setListener(OnViewInteractionListener l) {
        mListener = l;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setColor(int color) {
        mSignalPaint.setColor(color);
    }

    public void setYRange(float ymin, float ymax) {
        // Upper bound, can be divided by 10
        mYTicsMax = ymax;
        // Lower bound, can be divided by 10
        mYTicsMin = ymin;
    }

    public void setCircleVal(float val, int index) {
        // Upper bound, can be divided by 10
        mTrueCircleVal[index] = val;
    }

    public void setYticsUnit(String unit) {
        mYticsUnit = unit;
    }

    public void setData(float[] data, int index) {
        mSize = data.length;

        /** Y axis */
        //float[] dataY = mDataYList.get(index);
        float[] dataY = null; // workaround for now for variable plot length
        if (dataY == null) {
            dataY = new float[mSize];
            mDataYList.set(index, dataY);
        }
        for (int i = 0; i < mSize; i++) {
            float v = data[i];
            if (v != -1000f) {
                // Normalization
                v -= mYTicsMin;
                // Scale to actual height of this view and adjust to the frame
                v = mAxesBottom - (mAxesBottom - mAxesTop) * v / (mYTicsMax - mYTicsMin);
            }
            dataY[i] = v;
        }

        /** X axis */
//        float[] dataX = mDataXList.get(index);
        float[] dataX = null; // workaround for now for variable plot length
        if (dataX == null) {
            dataX = new float[mSize];
            mDataXList.set(index, dataX);
        }
        for (int i = 0; i < mSize; i++) {
            dataX[i] = (float) i / mSize * (mAxesRight - mAxesLeft) + mAxesLeft;
        }

        /** Path **/
        if (mSignalPathList.get(index) == null) {
            mSignalPathList.set(index, new Path());
        }
    }

    public void draw(boolean isRealTime) {
        mDrawRealTime = isRealTime;
        invalidate();
    }

    private void drawYTics(Canvas c) {
        float valueGap = (mYTicsMax - mYTicsMin) / mYTicsNum;
        float spaceGap = (mAxesBottom - mAxesTop) / mYTicsNum;
        float bottomPadding = 5;
        for (int i = 0; i < mYTicsNum + 1; i += 2) {
            String text = String.format("%d", (int) (mYTicsMax - valueGap * i));
            text += mYticsUnit;
            // String text = Float.toString((mYTicsMax - valueGap * i));
            float width = mTicsPaint.measureText(text);
            c.drawText(text, mAxesLeft - width / 3, mAxesTop + spaceGap * i
                    - bottomPadding, mTicsPaint);
        }
    }

    private void drawXTics(Canvas c) {
    }

    public void setText(String text) {
        mText = text;
        invalidate();
    }

    private void drawText(Canvas c) {
        // Draw title
        float spaceGap = (mAxesBottom - mAxesTop) / mYTicsNum;
        float bottomPadding = 5;
        float textWidth = mCirclePaint.measureText(mText);
        c.drawText(mText, mAxesRight - textWidth, mAxesTop + spaceGap * 4f
                - bottomPadding, mCirclePaint);
    }

    public void setTitlePaint(char color) {
        if (color == 'b') {
            mTitlePaint.setColor(getResources().getColor(android.R.color.holo_blue_dark));
        } else {
            mTitlePaint.setColor(getResources().getColor(android.R.color.white));
        }
    }

    private void drawTitle(Canvas c) {
        // Draw title
        float width = mAxesRight - mAxesLeft;

        float textWidth = mTitlePaint.measureText(mTitle);
        float remainWidth = width - textWidth;
        c.drawText(mTitle, mAxesLeft + remainWidth / 2 + textWidth / 2,
                mAxesTop + mTitleMarginBottom, mTitlePaint);
    }

    private void drawAxes(Canvas c) {
        // Mark lines for vertical axis
        float horizontalMarkLineGap = (mAxesBottom - mAxesTop) / mYTicsNum;
        for (int i = 0; i < mYTicsNum; i++) {
            float y = mAxesTop + horizontalMarkLineGap * i;
            c.drawLine(mAxesLeft, y, mAxesRight, y, mAxesMarkPaint);
        }
        drawYTics(c);

        // Horizontal axis
        float horizontalAxisLength = mAxesRight - mAxesLeft;
        c.drawLine(mAxesLeft, mAxesBottom, mAxesRight, mAxesBottom, mAxesPaint);

        // Marks on horizontal axis
        float markLength = 5 * DENSITY;
        for (int i = 1; i < 10; i++) {
            float x = mYMarksSpace + horizontalAxisLength / 9 * i;
            float yTop = mHeight - mXMarksSpace - markLength / 2;
            float yBottom = mHeight - mXMarksSpace + markLength / 2;
            c.drawLine(x, yTop, x, yBottom, mAxesPaint);
        }

        drawXTics(c);
    }

    private void drawLegend(Canvas c) {
        c.drawRect(mAxesRight - 2, mAxesTop, mAxesRight, mAxesTop - 10,
                mAxesMarkPaint);

        String text = String.format("Test");
        // String text = Float.toString((mYTicsMax - valueGap * i));
        float width = mTicsPaint.measureText(text);
        c.drawText(text, mAxesRight - width, mAxesRight + width, mTicsPaint);
    }

    private void drawCursor(Canvas c) {
        Paint cursorPaint = new Paint();
        cursorPaint.setColor(getResources().getColor(android.R.color.black));
        cursorPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        cursorPaint.setStrokeWidth(5);
        cursorPaint.setPathEffect(new DashPathEffect(new float[]{20, 40}, 0));
        c.drawLine(mWidth / 2, mAxesTop, mWidth / 2, mAxesBottom, cursorPaint);

        cursorPaint.setStrokeWidth(2);
        c.drawLine(mWidth / 2, mAxesTop + 10, mWidth / 2 + 10, mAxesTop,
                cursorPaint);
        c.drawLine(mWidth / 2, mAxesTop + 10, mWidth / 2 - 10, mAxesTop,
                cursorPaint);

        c.drawLine(mWidth / 2, mAxesBottom - 10, mWidth / 2 + 10, mAxesBottom,
                cursorPaint);
        c.drawLine(mWidth / 2, mAxesBottom - 10, mWidth / 2 - 10, mAxesBottom,
                cursorPaint);
    }

    public void clearSignal(int signalNum) {
        Path p = mSignalPathList.get(signalNum);
        if (p != null) {
            mSignalPathList.set(signalNum, null);
        }
    }

    private void drawSignal(Canvas c) {
        for (int i = 0; i < mLineNum; i++) {
            Path p = mSignalPathList.get(i);
            if (p != null) {
                p.reset();

                float[] dataX = mDataXList.get(i);
                float[] dataY = mDataYList.get(i);
                int len = dataX.length > dataY.length ? dataY.length : dataX.length;
                if (len == 0) continue;
                float start_y = 0;
                float start_x = 0;
                /* Draw path */
                if (i == 6) {
                    mSignalPaint.setStyle(Paint.Style.FILL_AND_STROKE);

                    start_y = mAxesBottom;
                    p.moveTo(dataX[0], start_y);
                    p.lineTo(dataX[0], dataY[0]);
                } else {
                    // Draw normal line
                    mSignalPaint.setStyle(Paint.Style.STROKE);
                    p.moveTo(dataX[0], dataY[0]);
                }
                for (int j = 1; j < len; j++) {
                    if (dataY[j] == -1000f) {
                        // Draw flag
                        //Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        // Draw a vertical line
//                        p.lineTo(dataX[j], mYTicsMax);
//                        p.lineTo(dataX[j], mYTicsMin);
//                        p.lineTo(dataX[j], mYTicsMax);
//                        c.drawBitmap(bmp, dataX[j], mYTicsMin, mInnerMarkPaint);
                    } else {
                        p.lineTo(dataX[j], dataY[j]);
                    }
                }

                mSignalPaint.setColor(mColors[i]);
                c.drawPath(p, mSignalPaint);

                float circleX = 0, circleY = 0;
                /* Draw mark */
                if (mDrawRealTime) {
                    circleX = dataX[len - 1];
                    circleY = dataY[len - 1];
                } else {
                    circleX = dataX[len / 2];
                    circleY = dataY[len / 2];
                }
                if (circleX + circleY != 0 && mTrueCircleVal[i] != 0) {
                    c.drawCircle(circleX, circleY, INNER_MARK_RADIUS, mInnerMarkPaint);
                    c.drawCircle(circleX, circleY, INNER_MARK_RADIUS, mOuterMarkPaint);
//                    float unNorm = -(circleY - mAxesBottom)
//                            * (mYTicsMax - mYTicsMin) / (mAxesBottom - mAxesTop);
//                    unNorm += mYTicsMin;
                    float unNorm = mTrueCircleVal[i];
                    c.drawText(String.format("%.1f", unNorm), circleX, circleY - 3, mCirclePaint);
                }
            }
        }
    }

    @Override
    protected void onDraw(Canvas c) {
        super.onDraw(c);
        drawTitle(c);
        drawAxes(c);
        drawSignal(c);
        drawText(c);
        //drawCursor(c);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        Log.d("double tap", "ANDROID DOUBLE TAP");
        return false;
    }


    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
        try {
            float distance = motionEvent2.getX() - motionEvent.getX();
            // right to left swipe
            if (-distance > SWIPE_MIN_DISTANCE
                    && Math.abs(v) > SWIPE_THRESHOLD_VELOCITY) {
                Toast.makeText(getContext(), "Left Swipe",
                        Toast.LENGTH_SHORT).show();
            } else if (distance > SWIPE_MIN_DISTANCE
                    && Math.abs(v) > SWIPE_THRESHOLD_VELOCITY) {
                Toast.makeText(getContext(), "Right Swipe",
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            // nothing
        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
        return false;
    }

}
