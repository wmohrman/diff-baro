
/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.nordicsemi.nrfUARTv2;




import java.text.DateFormat;
import java.util.Date;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements RadioGroup.OnCheckedChangeListener,LocationListener {

    //    private static String FRONT_DEVICE = "E5:BE:A2:CC:75:22";
    private static String FRONT_DEVICE = "C5:7F:D5:D4:AD:24";

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    public static final String TAG = "nRFUART";
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    TextView mRemoteRssiVal;
    RadioGroup mRg;
    private int mState = UART_PROFILE_DISCONNECTED;
    private UartService mService = null;
    private BluetoothDevice mDevice = null;
    private BluetoothAdapter mBtAdapter = null;
    private ListView messageListView;
    private ArrayAdapter<String> listAdapter;
    private Button btnConnectDisconnect,btnSend,btnReset;
    //private EditText edtMessage;
    private LineChartView mGraph;
    private static final int CURVE_DATA_SIZE = 400;    // How many points will be showing in curve
    private float[] mGraphData1 = new float[CURVE_DATA_SIZE];
    private float[] mGraphData2 = new float[CURVE_DATA_SIZE];
    private float[] mGraphData3 = new float[CURVE_DATA_SIZE];
    private float[] mGraphData4 = new float[CURVE_DATA_SIZE];
    String[] devList = new String[4];
    LocationManager locationManager;
    private double mLatitude = 0;
    private double mLongitude = 0;
    private float mGpsAlt = 0;
    private float mGpsSpeed = 0;
    private double mTimeStamp = 0;
    private int mHpa1 = 0;
    private int mHpa2 = 0;
    private int mHpa3 = 0;
    private int mHpa4 = 0;
    private float mOffset1 = 0;
    private float mOffset2 = 0;
    private float mOffset3 = 0;
    private float mOffset4 = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }


        // Check log folder
        if (!FileUtil.checkFolder()) {
            Toast.makeText(this, "Log folder has problem", Toast.LENGTH_SHORT);
        }
        // Init logger
        DataLogger.setFileTimeStamp(FileUtil.getCurrentTimestampForFile());
        String username = "wyatt";
        DataLogger.setUserName(username);

        messageListView = (ListView) findViewById(R.id.listMessage);
        listAdapter = new ArrayAdapter<String>(this, R.layout.message_detail);
        messageListView.setAdapter(listAdapter);
        messageListView.setDivider(null);
        btnConnectDisconnect=(Button) findViewById(R.id.btn_select);
        btnSend=(Button) findViewById(R.id.sendButton);
        btnReset=(Button) findViewById(R.id.sendButtonReset);
        //edtMessage = (EditText) findViewById(R.id.sendText);
        mGraph = (LineChartView) findViewById(R.id.graph);
        service_init();

        // GPS
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, this);
        Toast.makeText(this, "GPS ENABLED",Toast.LENGTH_LONG).show();

        // Handle Disconnect & Connect button
        btnConnectDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBtAdapter.isEnabled()) {
                    Log.i(TAG, "onClick - BT not enabled yet");
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                } else {
                    if (btnConnectDisconnect.getText().equals("Connect")) {

                        //Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices

                        Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                        startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
                    } else {
                        //Disconnect button pressed
                        if (mDevice != null) {
                            mService.disconnect();

                        }
                    }
                }
            }
        });
        // Handle Send button
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOffset1 = Util.mean(mGraphData1);
                mOffset2 = Util.mean(mGraphData2);
                mOffset3 = Util.mean(mGraphData3);
                mOffset4 = Util.mean(mGraphData4);

                btnReset.setEnabled(true);
                btnSend.setEnabled(false);
            }
        });

        // Handle Reset button
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOffset1 = 0;
                mOffset2 = 0;
                mOffset3 = 0;
                mOffset4 = 0;
                btnReset.setEnabled(false);
                btnSend.setEnabled(true);
            }
        });
        // Set initial UI state

    }

    //UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }

        }

        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            mService = null;
        }
    };

    private Handler mHandler = new Handler() {
        @Override

        //Handler events that received from UART service 
        public void handleMessage(Message msg) {

        }
    };
    static float diff_press1 = 0;
    static float diff_press2 = 0;
    private final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            final Intent mIntent = intent;
            //*********************//
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_CONNECT_MSG");
                        //btnConnectDisconnect.setText("Disconnect");
                        //edtMessage.setEnabled(true);
                        btnReset.setEnabled(false);
                        btnSend.setEnabled(true);
                        ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName() + " - ready");
                        listAdapter.add("[" + currentDateTimeString + "] Connected to: " + mDevice.getName() + "\n" + " MAC: " + mDevice.getAddress() + "\n");
                        messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                        mState = UART_PROFILE_CONNECTED;
                    }
                });
            }

            //*********************//
            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_DISCONNECT_MSG");
                        btnConnectDisconnect.setText("Connect");
                        //edtMessage.setEnabled(false);
                        btnSend.setEnabled(false);
                        ((TextView) findViewById(R.id.deviceName)).setText("Not Connected");
                        listAdapter.add("["+currentDateTimeString+"] Disconnected to: "+ mDevice.getName() + "\n" + " MAC: "+mDevice.getAddress() + "\n");
                        messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                        mState = UART_PROFILE_DISCONNECTED;
                        mService.close();
                        //setUiState();

                    }
                });
            }


            //*********************//
            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mService.enableTXNotification();
            }
            //*********************//
            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {

                final String deviceId = intent.getStringExtra(UartService.DEVICE_ID);
                final int total_baro = intent.getIntExtra(UartService.PRESSURE, -1);
                final int baro_temp = intent.getIntExtra(UartService.TEMPERATURE, -1);


                // Assign places to connected devices (up to 3 for now)
                // Assign front device first
                if(devList[0] == null) {
                    Log.d("DEV", "id = " + deviceId);
                    if(true){
                    //if(deviceId.equals(FRONT_DEVICE)) {
                        devList[0] = deviceId;
                        for (int i = 0; i < CURVE_DATA_SIZE; i++) {
                            mGraphData1[i] = 0;
                            mGraphData2[i] = 0;
                            mGraphData3[i] = 0;
                            mGraphData4[i] = 0;
                        }
                    }
                }
                else if(devList[1] == null) {
                    Log.d("DEV", "id = " + deviceId);
                    if(!deviceId.equals(devList[0])) {
                        devList[1] = deviceId;
                        for (int i = 0; i < CURVE_DATA_SIZE; i++) {
                            mGraphData1[i] = 0;
                            mGraphData2[i] = 0;
                            mGraphData3[i] = 0;
                            mGraphData4[i] = 0;
                        }
                    }
                }
                else if(devList[2] == null) {
                    Log.d("DEV", "id = " + deviceId);
                    if(!deviceId.equals(devList[0]) && !deviceId.equals(devList[1])) {
                        devList[2] = deviceId;
                        for (int i = 0; i < CURVE_DATA_SIZE; i++) {
                            mGraphData1[i] = 0;
                            mGraphData2[i] = 0;
                            mGraphData3[i] = 0;
                            mGraphData4[i] = 0;
                        }
                    }
                }
                else if(devList[3] == null) {
                    Log.d("DEV", "id = " + deviceId);
                    if(!deviceId.equals(devList[0]) && !deviceId.equals(devList[1]) && !deviceId.equals(devList[2])) {
                        devList[3] = deviceId;
                        for (int i = 0; i < CURVE_DATA_SIZE; i++) {
                            mGraphData1[i] = 0;
                            mGraphData2[i] = 0;
                            mGraphData3[i] = 0;
                            mGraphData4[i] = 0;
                        }
                    }
                }


                int deviceNum = 0;
                if (deviceId.equals(devList[0])) {
                    deviceNum = 0;
                } else if (deviceId.equals(devList[1])) {
                    deviceNum = 1;
                }else if (deviceId.equals(devList[2])) {
                    deviceNum = 2;
                }else if (deviceId.equals(devList[3])) {
                    deviceNum = 3;
                }
                //Log.d("DEV", "NUM = " + deviceNum);
                if (deviceNum == 0) {
                    for (int i = 0; i < CURVE_DATA_SIZE - 1; i++) {
                        mGraphData1[i] = mGraphData1[i + 1];
                    }
                    mHpa1 = total_baro;
                    //mGraphData1[CURVE_DATA_SIZE - 1] = mHpa1 - mOffset1;
                    diff_press1 = 1 / 256.0f * (mHpa1 - mHpa2);
                    mGraphData1[CURVE_DATA_SIZE - 1] = diff_press1 - mOffset1;
                }
                if (deviceNum == 1) {
                    for (int i = 0; i < CURVE_DATA_SIZE - 1; i++) {
                        mGraphData2[i] = mGraphData2[i + 1];
                    }
                    mHpa2 = total_baro;
                    //mGraphData2[CURVE_DATA_SIZE - 1] = mHpa2 - mOffset2;
                    diff_press2 = 1 / 256.0f * (mHpa3 - mHpa4);
                    mGraphData2[CURVE_DATA_SIZE - 1] = diff_press2 - mOffset2;
                }
                if(deviceNum == 2) {
                    for (int i = 0; i < CURVE_DATA_SIZE - 1; i++) {
                        mGraphData3[i] = mGraphData3[i + 1];
                    }
                    mHpa3 = total_baro;
                    mGraphData3[CURVE_DATA_SIZE - 1] = mHpa3 - mOffset3;
                }
                if (deviceNum == 3) {
                    for (int i = 0; i < CURVE_DATA_SIZE - 1; i++) {
                        mGraphData4[i] = mGraphData4[i + 1];
                    }
                    mHpa4 = total_baro;
                    mGraphData4[CURVE_DATA_SIZE - 1] = mHpa4 - mOffset4;
                }

                float sec_mean = 0;
                for (int i = 1; i < 21; i++)
                    sec_mean += mGraphData1[CURVE_DATA_SIZE - i] / 20.0f;

                float allMin = Util.min(new float[]{Util.min(mGraphData1), Util.min(mGraphData2)});
                float allMax = Util.max(new float[]{Util.max(mGraphData1), Util.max(mGraphData2)});
                mGraph.setYRange(allMin, allMax);
                mGraph.setData(mGraphData1, 0);
                mGraph.setData(mGraphData2, 1);


//                float allMin = Util.min(new float[]{Util.min(mGraphData1), Util.min(mGraphData2),
//                        Util.min(mGraphData3),Util.min(mGraphData4)});
//                float allMax = Util.max(new float[]{Util.max(mGraphData1), Util.max(mGraphData2),
//                        Util.max(mGraphData3),Util.max(mGraphData4)});
//                mGraph.setYRange(allMin, allMax);
//                mGraph.setData(mGraphData1, 0);
//                mGraph.setData(mGraphData2, 1);
//                mGraph.setData(mGraphData3, 2);
//                mGraph.setData(mGraphData4, 3);

                mGraph.setTitle(String.format("%.2f", sec_mean) + " Pa");

                mGraph.draw(true);
                DataLogger.recordDeviceBaro(mHpa1, mHpa2,mHpa3,mHpa4, mGpsSpeed, mGpsAlt, System.currentTimeMillis());
            }
            //*********************//
            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)){
                showMessage("Device doesn't support UART. Disconnecting");
                //mService.disconnect();
            }


        }
    };

    private void service_init() {
        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(this).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        locationManager.removeUpdates(this);
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(UARTStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        }
        unbindService(mServiceConnection);
        mService.stopSelf();
        mService= null;

    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onResume - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);

                    Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
                    ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - connecting");
                    mService.connect(deviceAddress);


                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            default:
                Log.e(TAG, "wrong request code");
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }


    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        if (mState == UART_PROFILE_CONNECTED) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            showMessage("nRFUART's running in background.\n             Disconnect to exit");
        }
        else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.popup_title)
                    .setMessage(R.string.popup_message)
                    .setPositiveButton(R.string.popup_yes, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.popup_no, null)
                    .show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLatitude = (location.getLatitude());
        mLongitude = (location.getLongitude());
        mGpsAlt = (float) location.getAltitude();
        mGpsSpeed = location.getSpeed();
        mTimeStamp = System.currentTimeMillis();

//        runOnUiThread(new Runnable() {
//            public void run() {
//                try {
//                    String text = String.format("MPH = %.2f",mGpsSpeed * 2.23);//new String(txValue, "UTF-8");
//                    String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
//                    listAdapter.add("[" + currentDateTimeString + "] RX: " + text);
//                    messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
//
//                } catch (Exception e) {
//                    Log.e(TAG, e.toString());
//                }
//            }
//        });

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
