package com.nordicsemi.nrfUARTv2;

import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by cnbuff410 on 9/25/13.
 */
public class FileUtil {
    public static final String ROOT_PATH = Environment.getExternalStorageDirectory().getPath() +
            "/Newton/";
    public static final String MUSIC_PATH = Environment.getExternalStorageDirectory().getPath() +
            "/NewtonMusic/";
    public static final String AUDIO_RECORD_FILE = ROOT_PATH + "record.3gp";
    public static final String AUDIO_PLAY_FILE = ROOT_PATH + "play.3gp";

    public static boolean checkFolder() {
        File folder = new File(ROOT_PATH);
        if (!folder.exists()) {
            folder.mkdir();
        }
        if (!folder.canWrite()) {
            return false;
        }
        return true;
    }

    public static String getCurrentTimestampForFile() {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        Timestamp currTime = new Timestamp(now.getTime());
        String fileTimestamp = new SimpleDateFormat("yyyy-MM-dd-kk-mm-ss")
                .format(currTime);
        return fileTimestamp;
    }

    public static String getCurrentTimestamp() {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        Timestamp currTime = new Timestamp(now.getTime());
        String timestamp = new SimpleDateFormat("kk:mm:ss").format(currTime);
        return timestamp;
    }

    /**
     * Read the whole file content and return as a string.
     * Only use it for SMALL file!!!
     *
     * @param filePath path to file
     * @return File content in a string
     * @throws Exception
     */
    public static String getStringFromFile(String filePath) throws IOException {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        fin.close();
        return sb.toString();
    }

    /**
     * Read the whole file content and return as a string array list.
     * Only use it for SMALL file!!!
     *
     * @param filePath path to file
     * @return File content in a string arraylist, one item is one line
     * @throws Exception
     */
    public static ArrayList<String> getStringListFromFile(String filePath) throws IOException {
        File fl = new File(filePath);
        if (!fl.exists()) {
            return null;
        }
        FileInputStream fin = new FileInputStream(fl);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
        ArrayList<String> list = new ArrayList<String>();
        String line = null;
        while ((line = reader.readLine()) != null) {
            list.add(line);
        }
        fin.close();
        return list;
    }

    /**
     * Read the whole file content and return as a byte array
     * Only use it for SMALL file!!!
     *
     * @param filePath path to file
     * @return File content in a byte array
     * @throws Exception
     */
    public static byte[] getByteArrayFromFile(String filePath) throws IOException {
        File file = new File(filePath);
        FileInputStream fin = null;
        // create FileInputStream object
        fin = new FileInputStream(file);
        byte[] fileContent = new byte[(int) file.length()];
        // Reads up to certain bytes of data from this input stream into an array of bytes.
        fin.read(fileContent);
        //create string from byte array
        String s = new String(fileContent);
        System.out.println("File content: " + s);
        fin.close();
        return fileContent;
    }

    public static void writeByteArrayToFile(String path, byte[] data) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path));
        bos.write(data);
        bos.flush();
        bos.close();
    }

    /**
     * Return a list of path for all files newer than given date
     *
     * @param calendar Date to be compared
     * @return a list of path
     */
    public static ArrayList<String> getPathForFileAfter(GregorianCalendar calendar) {
        File root = new File(ROOT_PATH);
        File[] files = root.listFiles();
        ArrayList<String> validFiles = new ArrayList<String>();

        if (files == null || files.length == 0) {
            return validFiles;
        }

        for (File f : files) {
            if (!f.isFile()) {
                continue;
            }
            String fName = f.getName();
            if (fName.endsWith(".device_acc") || fName.endsWith(".device_gyro") ||
                    fName.endsWith("gps") || fName.endsWith("result") ) {
                String timestamp = fName.split("_")[0];
                String[] ts = timestamp.split("-");
                if (ts.length != 6) continue;
                GregorianCalendar localCalendar = new GregorianCalendar();
                localCalendar.set(Integer.parseInt(ts[0]), Integer.parseInt(ts[1]),
                        Integer.parseInt(ts[2]), Integer.parseInt(ts[3]),
                        Integer.parseInt(ts[4]), Integer.parseInt(ts[5]));

                // 10 seconds gap
                if (localCalendar.getTimeInMillis() > calendar.getTimeInMillis() + 10000) {
                    validFiles.add(ROOT_PATH + fName);
                }
            }
        }
        return validFiles;
    }

    public static void writeDataToTCX(String filepath) {
        if (StepList.sRtTimestampList.size() <= 0) return;
        String startTimestamp = DataLogger.getFileTimeStamp();
        String[] timeItems = startTimestamp.split("-");
        GregorianCalendar start = new GregorianCalendar(
                Integer.valueOf(timeItems[0]),
                Integer.valueOf(timeItems[1]) - 1,
                Integer.valueOf(timeItems[2]),
                Integer.valueOf(timeItems[3]),
                Integer.valueOf(timeItems[4]),
                Integer.valueOf(timeItems[5])
        );

        Calendar current = GregorianCalendar.getInstance();
        long duration = current.getTimeInMillis() / 1000 - start.getTimeInMillis() / 1000;

        try {
            String formatString = "yyyy-MM-ddThh:mm:ssZ";
            FileOutputStream myFile = new FileOutputStream(filepath);

            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);

            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag("", "TrainingCenterDatabase");
            xmlSerializer.attribute("", "xmlns",
                    "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2");
            xmlSerializer.attribute("", "xmlns:xsi",
                    "http://www.w3.org/2001/XMLSchema-instance");
            xmlSerializer.attribute("", "xsi:schemaLocation",
                    "http://www.garmin.com/xmlschemas/ActivityExtension/v1 http://www.garmin" +
                            ".com/xmlschemas/ActivityExtensionv1.xsd http://www.garmin" +
                            ".com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin" +
                            ".com/xmlschemas/TrainingCenterDatabasev2.xsd"
            );
            xmlSerializer.startTag("", "Activities");
            xmlSerializer.startTag("", "Activity");
            xmlSerializer.attribute("", "Sport", "Running");
            xmlSerializer.startTag("", "Id");
            xmlSerializer.text((String) DateFormat.format(formatString,
                    StepList.sRtTimestampList.get(0)));
            xmlSerializer.endTag("", "Id");
            xmlSerializer.startTag("", "Lap");
            xmlSerializer.attribute("", "StartTime", (String) DateFormat.format(formatString,
                    StepList.sRtTimestampList.get(0)));
            xmlSerializer.startTag("", "TotalTimeSeconds");
            xmlSerializer.text(Long.toString(duration));
            xmlSerializer.endTag("", "TotalTimeSeconds");
            xmlSerializer.startTag("", "DistanceMeters");
            xmlSerializer.text(Float.toString(StepList.sRtDeviceDistance));
            xmlSerializer.endTag("", "DistanceMeters");
            xmlSerializer.startTag("", "MaximumSpeed");
            xmlSerializer.text(Float.toString(Util.max(StepList.sRtDeviceSpeedList)));
            xmlSerializer.endTag("", "MaximumSpeed");
            xmlSerializer.startTag("", "Calories");
            xmlSerializer.text(Float.toString(StepList.getCalories()));
            xmlSerializer.endTag("", "Calories");

            xmlSerializer.startTag("", "Track");
            for (int i = 0; i < StepList.sRtDeviceDistanceList.size(); i++) {
                xmlSerializer.startTag("", "Trackpoint");
                xmlSerializer.startTag("", "Time");
                xmlSerializer.text((String) DateFormat.format(formatString,
                        StepList.sRtTimestampList.get(i)));
                xmlSerializer.endTag("", "Time");
                xmlSerializer.startTag("", "DistanceMeters");
                xmlSerializer.text(Float.toString(StepList.sRtDeviceDistanceList.get(i)));
                xmlSerializer.endTag("", "DistanceMeters");
                xmlSerializer.startTag("", "SensorState");
                xmlSerializer.text("Present");
                xmlSerializer.endTag("", "SensorState");

                xmlSerializer.startTag("", "Extensions");
                xmlSerializer.startTag("", "ActivityTrackpointExtension");
                xmlSerializer.attribute("", "xmlns",
                        "http://www.garmin.com/xmlschemas/ActivityExtension/v1");
                xmlSerializer.attribute("", "SourceSensor", "Footpod");
                xmlSerializer.startTag("", "Speed");
                xmlSerializer.text(Float.toString(StepList.sRtDeviceSpeedList.get(i)));
                xmlSerializer.endTag("", "Speed");
                xmlSerializer.endTag("", "ActivityTrackpointExtension");
                xmlSerializer.endTag("", "Extensions");

                xmlSerializer.endTag("", "Trackpoint");
            }
            xmlSerializer.endTag("", "Track");

            xmlSerializer.startTag("", "Extensions");
            xmlSerializer.startTag("", "ActivityLapExtension");
            xmlSerializer.attribute("", "xmlns",
                    "http://www.garmin.com/xmlschemas/ActivityExtension/v1");
            xmlSerializer.startTag("", "AvgSpeed");
            xmlSerializer.text(Float.toString(Util.mean(StepList.sRtDeviceSpeedList)));
            xmlSerializer.endTag("", "AvgSpeed");
            xmlSerializer.startTag("", "AvgRunCadence");
            xmlSerializer.text(Float.toString(Util.mean(StepList.sRtSpmList)));
            xmlSerializer.endTag("", "AvgRunCadence");
            xmlSerializer.startTag("", "MaxRunCadence");
            xmlSerializer.text(Float.toString(Util.max(StepList.sRtSpmList)));
            xmlSerializer.endTag("", "MaxRunCadence");
            xmlSerializer.startTag("", "Steps");
            xmlSerializer.text(Float.toString(StepList.sRunSteps));
            xmlSerializer.endTag("", "Steps");
            xmlSerializer.endTag("", "ActivityLapExtension");
            xmlSerializer.endTag("", "Extensions");
            xmlSerializer.endTag("", "Lap");

            xmlSerializer.startTag("", "Creator");
            xmlSerializer.attribute("", "xsi:type", "Device_t");
            xmlSerializer.startTag("", "Name");
            xmlSerializer.text("Test");
            xmlSerializer.endTag("", "Name");
            xmlSerializer.startTag("", "UnitId");
            xmlSerializer.text("12");
            xmlSerializer.endTag("", "UnitId");
            xmlSerializer.startTag("", "ProductId");
            xmlSerializer.text("15");
            xmlSerializer.endTag("", "ProductId");
            xmlSerializer.startTag("", "Version");
            xmlSerializer.startTag("", "VersionMajor");
            xmlSerializer.text("0");
            xmlSerializer.endTag("", "VersionMajor");
            xmlSerializer.startTag("", "VersionMinor");
            xmlSerializer.text("0");
            xmlSerializer.endTag("", "VersionMinor");
            xmlSerializer.startTag("", "BuildMajor");
            xmlSerializer.text("0");
            xmlSerializer.endTag("", "BuildMajor");
            xmlSerializer.startTag("", "BuildMinor");
            xmlSerializer.text("0");
            xmlSerializer.endTag("", "BuildMinor");
            xmlSerializer.endTag("", "Version");
            xmlSerializer.endTag("", "Creator");

            xmlSerializer.endTag("", "Activity");
            xmlSerializer.endTag("", "Activities");
            xmlSerializer.endTag("", "TrainingCenterDatabase");
            xmlSerializer.endDocument();

            Log.d("Kun1", writer.toString());
            myFile.write(writer.toString().getBytes());
            myFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
