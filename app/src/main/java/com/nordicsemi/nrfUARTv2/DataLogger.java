package com.nordicsemi.nrfUARTv2;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by cnbuff410 on 9/25/13.
 */
public class DataLogger {
    private static boolean sRecordResult = true;
    //private static String sUserName = "Test";
    //private static final String sVersion = "v5";
    private static String sFileTimestamp = "";
    private static String sBatteryLogFileName = "battery.log";
    private static String sPhoneOprLogFileName = "phoneOpr.log";
    private static String sDeviceLogFileName = "deviceOpr.log";

    private static ArrayList<Integer> sBaroPres1 = new ArrayList<Integer>();
    private static ArrayList<Integer> sBaroPres2 = new ArrayList<Integer>();
    private static ArrayList<Integer> sBaroPres3 = new ArrayList<Integer>();
    private static ArrayList<Integer> sBaroPres4 = new ArrayList<Integer>();

    private static ArrayList<Long> sHr = new ArrayList<Long>();
    private static ArrayList<Float> sGpsSpeed = new ArrayList<Float>();
    private static ArrayList<Float> sGpsAlt = new ArrayList<Float>();

    private static ArrayList<Integer> sAccX = new ArrayList<Integer>();
    private static ArrayList<Integer> sAccY = new ArrayList<Integer>();
    private static ArrayList<Integer> sAccZ = new ArrayList<Integer>();
    private static ArrayList<Long> sAccTS = new ArrayList<Long>();
    private static ArrayList<Integer> sGyroX = new ArrayList<Integer>();
    private static ArrayList<Integer> sGyroY = new ArrayList<Integer>();
    private static ArrayList<Integer> sGyroZ = new ArrayList<Integer>();

    private static ArrayList<Integer> sWalkCnt = new ArrayList<Integer>();
    private static ArrayList<Integer> sRunCnt = new ArrayList<Integer>();
    private static ArrayList<Float> sStrideTime = new ArrayList<Float>();
    private static ArrayList<Float> sGroundTime = new ArrayList<Float>();
    private static ArrayList<Float> sDistanceY = new ArrayList<Float>();
    private static ArrayList<Float> sDistanceZ = new ArrayList<Float>();
    private static ArrayList<Float> sImpactY = new ArrayList<Float>();
    private static ArrayList<Float> sImpactZ = new ArrayList<Float>();
    private static ArrayList<Float> sCalories = new ArrayList<Float>();
    private static ArrayList<Float> sFatigue = new ArrayList<Float>();
    private static ArrayList<Long> sResultTimestamp = new ArrayList<Long>();
    private static ArrayList<String> sDeviceOprs = new ArrayList<String>();

    private static ArrayList<Double> sLats = new ArrayList<Double>();
    private static ArrayList<Double> sLngs = new ArrayList<Double>();
    private static ArrayList<Double> sAlts = new ArrayList<Double>();
    private static ArrayList<Double> sSpeedsGps = new ArrayList<Double>();
    private static ArrayList<Double> sDistancesGps = new ArrayList<Double>();
    private static ArrayList<Double> sSpeedsDevice = new ArrayList<Double>();
    private static ArrayList<Double> sDistancesDevice = new ArrayList<Double>();
    private static ArrayList<Long> sLocTimestamp = new ArrayList<Long>();


    private static ArrayList<Double> sPhoneAccX = new ArrayList<Double>();
    private static ArrayList<Double> sPhoneAccY = new ArrayList<Double>();
    private static ArrayList<Double> sPhoneAccZ = new ArrayList<Double>();
    private static ArrayList<Long> sPhoneTimestamp = new ArrayList<Long>();

    public DataLogger() {
    }

    public static void setUserName(String name) {
        //sUserName = name + "-" + sVersion;
    }

    public static void setFileTimeStamp(String ts) {
        sFileTimestamp = ts;
    }

    public static String getFileTimeStamp() {
        return sFileTimestamp;
    }

    public static String getResultFileName() {
        return FileUtil.ROOT_PATH + sFileTimestamp + ".result";
    }

    // getVideoFileName always return video file name corresponding to the file name used by result
    // recording in the same session
    public static String getVideoFileName() {
        return FileUtil.ROOT_PATH + sFileTimestamp  + ".mp4";
    }

    // getBaseFileName always return base file name without suffix corresponding to the file name
    // used by result and video recording in the same session
    public static String getBaseFileName() {
        return FileUtil.ROOT_PATH + sFileTimestamp;
    }

    public static void recordDeviceAcc(int accX, int accY, int accZ) {
        sAccX.add(accX);
        sAccY.add(accY);
        sAccZ.add(accZ);
        sAccTS.add(System.currentTimeMillis());
        if (sAccX.size() > 1000) {
            flushDeviceAcc();
        }
    }

    public static void recordDeviceBaro(int pres1, int pres2, int pres3, int pres4, float gps_speed, float gps_alt, long hr) {
        sBaroPres1.add(pres1);
        sBaroPres2.add(pres2);
        sBaroPres3.add(pres3);
        sBaroPres4.add(pres4);
        sHr.add(hr);
        sGpsSpeed.add(gps_speed);
        sGpsAlt.add(gps_alt);

        if (sBaroPres1.size() > 20) {
            flushDevicePresTemp();
        }

    }

    public static void recordDeviceGyro(int gyroX, int gyroY, int gyroZ) {
        sGyroX.add(gyroX);
        sGyroY.add(gyroY);
        sGyroZ.add(gyroZ);
        if (sGyroX.size() > 1000) {
            flushDeviceGyro();
        }
    }

    public static void recordDeviceResult(int walkCnt, int runCnt, float strideTime,
                                          float groundTime, float distanceY, float distanceZ,
                                          float impactY, float impactZ, float calories,
                                          float fatigue) {
        if (!sRecordResult) return;

        sWalkCnt.add(walkCnt);
        sRunCnt.add(runCnt);
        sStrideTime.add(strideTime);
        sGroundTime.add(groundTime);
        sDistanceY.add(distanceY);
        sDistanceZ.add(distanceZ);
        sImpactY.add(impactY);
        sImpactZ.add(impactZ);
        sCalories.add(calories);
        sFatigue.add(fatigue);
        sResultTimestamp.add(System.currentTimeMillis());

        float[] sizes = {sWalkCnt.size(), sRunCnt.size(), sStrideTime.size(), sGroundTime.size()
                , sDistanceY.size(), sDistanceZ.size(), sImpactY.size(), sImpactZ.size()
                , sResultTimestamp.size(), sCalories.size(), sFatigue.size()};
        int size = (int) Util.min(sizes);

        if (size > 20) {
            flushResult();
        }
    }

    public static void recordLocation(double lat, double lng, double alt, double speedGps,
                                      double distGps, double speedDevice, double disDevice,
                                      long timestamp) {
        sLats.add(lat);
        sLngs.add(lng);
        sAlts.add(alt);
        sSpeedsGps.add(speedGps);
        sDistancesGps.add(distGps);
        sSpeedsDevice.add(speedDevice);
        sDistancesDevice.add(disDevice);
        sLocTimestamp.add(timestamp);

        float[] sizes = {sLats.size(), sLngs.size(), sAlts.size(), sSpeedsGps.size(),
                sDistancesGps.size(), sSpeedsDevice.size(), sDistancesDevice.size(),
                sLocTimestamp.size()};
        int minSize = (int) Util.min(sizes);

        if (minSize > 20) {
            flushLocation();
        }
    }

    public static void recordPhoneAcc(double x, double y, double z, long timestamp) {
        sPhoneAccX.add(x);
        sPhoneAccY.add(y);
        sPhoneAccZ.add(z);
        sPhoneTimestamp.add(timestamp);

        float[] sizes = {sPhoneAccX.size(), sPhoneAccY.size(), sPhoneAccZ.size(),
                sPhoneTimestamp.size()};
        int minSize = (int) Util.min(sizes);

        if (minSize > 2000) {
            flushPhoneAcc();
        }
    }

    public static void recordPhoneOpr(String msg) {
        if (!BuildConfig.DEBUG) return;

        Log.d("DEBUG", FileUtil.getCurrentTimestamp() + ":" + msg);
        try {
            BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                    "/" + sPhoneOprLogFileName), true));
            wFile.write(System.currentTimeMillis() + ":" + msg + "\n");
            wFile.close();
        } catch (IOException e) {
            Log.e("Kun", "Could not write file " + e.getMessage());
        }
    }

    public static void recordBattery(int level) {
        if (!BuildConfig.DEBUG) return;

        Log.d("DEBUG", FileUtil.getCurrentTimestamp() + ":" + level);
        try {
            BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                    "/" + sBatteryLogFileName), true));
            wFile.write(System.currentTimeMillis() + ":" + level + "\n");
            wFile.close();
        } catch (IOException e) {
            Log.e("Kun", "Could not write file " + e.getMessage());
        }
    }

    public static void recordDeviceLog(String deviceLog) {
        sDeviceOprs.add(deviceLog);
        if (sDeviceOprs.size() > 100) {
            flushDeviceLog();
        }
    }

    public static void recordGPSResult(String resultString) {

        try {
            BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                    sFileTimestamp + ".gps"), true));
            wFile.write(resultString);
            wFile.close();
        } catch (IOException e) {
            Log.d("Kun", "Could not write file " + e.getMessage());
        }
    }

    public static void recordBaroResult(String resultString) {
        try {
            BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                    sFileTimestamp + ".barometer"), true));
            wFile.write(resultString);
            wFile.close();
        } catch (IOException e) {
            Log.d("Kun", "Could not write file " + e.getMessage());
        }
    }

//    public static boolean checkFolder() {
//        File folder = new File(FileUtil.ROOT_PATH );
//        if (!folder.exists()) {
//            folder.mkdir();
//        }
//        if (!folder.canWrite()) {
//            return false;
//        }
//        return true;
//    }

    private static void flushDeviceAcc() {
        float[] sizes = {sAccX.size(), sAccY.size(), sAccZ.size(), sAccTS.size()};
        int size = (int) Util.min(sizes);
        if (size > 0) {
            int[] xArray = new int[size];
            int[] yArray = new int[size];
            int[] zArray = new int[size];
            long[] tsArray = new long[size];
            for (int i = 0; i < size; i++) {
                xArray[i] = sAccX.get(i);
                yArray[i] = sAccY.get(i);
                zArray[i] = sAccZ.get(i);
                tsArray[i] = sAccTS.get(i);
            }

            new DeviceAccRecordingTask(xArray, yArray, zArray,tsArray, size).execute();
            sAccX.clear();
            sAccY.clear();
            sAccZ.clear();
            sAccTS.clear();
        }
    }

    private static void flushDeviceGyro() {
        float[] sizes = {sGyroX.size(), sGyroY.size(), sGyroZ.size()};
        int size = (int) Util.min(sizes);
        if (size > 0) {
            int[] xArray = new int[size];
            int[] yArray = new int[size];
            int[] zArray = new int[size];
            for (int i = 0; i < size; i++) {
                xArray[i] = sGyroX.get(i);
                yArray[i] = sGyroY.get(i);
                zArray[i] = sGyroZ.get(i);
            }

            new DeviceGyroRecordingTask(xArray, yArray, zArray, size).execute();
            sGyroX.clear();
            sGyroY.clear();
            sGyroZ.clear();
        }
    }

    private static void flushDevicePresTemp() {
        float[] sizes = {sBaroPres1.size(),sBaroPres2.size(),sBaroPres3.size(),sBaroPres4.size()};
        int size = (int) Util.min(sizes);
        if (size > 0) {
            int[] presArray1 = new int[size];
            int[] presArray2 = new int[size];
            int[] presArray3 = new int[size];
            int[] presArray4 = new int[size];
            float[] gpsSpeedArray = new float[size];
            float[] gpsAltArray = new float[size];
            long[] hrArray = new long[size];
            for (int i = 0; i < size; i++) {
                presArray1[i] = sBaroPres1.get(i);
                presArray2[i] = sBaroPres2.get(i);
                presArray3[i] = sBaroPres3.get(i);
                presArray4[i] = sBaroPres4.get(i);
                gpsSpeedArray[i] = sGpsSpeed.get(i);
                gpsAltArray[i] = sGpsAlt.get(i);
                hrArray[i] = sHr.get(i);
            }
            new DeviceCntRecordingTask(presArray1,presArray2,presArray3,presArray4,gpsSpeedArray,gpsAltArray,hrArray, size).execute();
            sBaroPres1.clear();
            sBaroPres2.clear();
            sBaroPres3.clear();
            sBaroPres4.clear();
            sGpsSpeed.clear();
            sGpsAlt.clear();
            sHr.clear();
        }
    }

    private static void flushResult() {
        float[] sizes = {sWalkCnt.size(), sRunCnt.size(), sStrideTime.size(), sGroundTime.size()
                , sDistanceY.size(), sDistanceZ.size(), sImpactY.size(), sImpactZ.size()
                , sResultTimestamp.size(), sCalories.size(), sFatigue.size()};
        int size = (int) Util.min(sizes);

        if (size > 0) {
            int[] walkCntArray = new int[size];
            int[] runCntArray = new int[size];
            float[] strideTimeArray = new float[size];
            float[] groundTimeArray = new float[size];
            float[] distanceYArray = new float[size];
            float[] distanceZArray = new float[size];
            float[] impactYArray = new float[size];
            float[] impactZArray = new float[size];
            float[] caloriesArray = new float[size];
            float[] fatigueArray = new float[size];
            long[] timestampArray = new long[size];
            for (int i = 0; i < size; i++) {
                walkCntArray[i] = sWalkCnt.get(i);
                runCntArray[i] = sRunCnt.get(i);
                strideTimeArray[i] = sStrideTime.get(i);
                groundTimeArray[i] = sGroundTime.get(i);
                distanceYArray[i] = sDistanceY.get(i);
                distanceZArray[i] = sDistanceZ.get(i);
                impactYArray[i] = sImpactY.get(i);
                impactZArray[i] = sImpactZ.get(i);
                caloriesArray[i] = sCalories.get(i);
                fatigueArray[i] = sFatigue.get(i);
                timestampArray[i] = sResultTimestamp.get(i);
            }

            new ResultRecordingTask(walkCntArray, runCntArray, strideTimeArray, groundTimeArray,
                    distanceYArray, distanceZArray, impactYArray, impactZArray, caloriesArray,
                    fatigueArray, timestampArray, size).execute();
            sWalkCnt.clear();
            sRunCnt.clear();
            sStrideTime.clear();
            sGroundTime.clear();
            sDistanceY.clear();
            sDistanceZ.clear();
            sImpactZ.clear();
            sImpactY.clear();
            sCalories.clear();
            sFatigue.clear();
            sResultTimestamp.clear();
        }
    }

    private static void flushLocation() {
        float[] sizes = {sLats.size(), sLngs.size(), sAlts.size(), sSpeedsGps.size(),
                sDistancesGps.size(), sSpeedsDevice.size(), sDistancesDevice.size(),
                sLocTimestamp.size()};
        int minSize = (int) Util.min(sizes);
        if (minSize > 0) {
            new LocationRecordingTask().execute();
        }
    }

    private static void flushPhoneAcc() {
        float[] sizes = {sPhoneAccX.size(), sPhoneAccY.size(), sPhoneAccZ.size(),
                sPhoneTimestamp.size()};
        int minSize = (int) Util.min(sizes);
        if (minSize > 0) {
            new PhoneAccRecordingTask().execute();
        }
    }

    public static void flushDeviceLog() {
        float size = sDeviceOprs.size();
        if (size > 0) {
            try {
                BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                        sFileTimestamp + "_" + sDeviceLogFileName), true));
                for (String line : sDeviceOprs) {
                    wFile.write(line);
                }
                wFile.close();
                sDeviceOprs.clear();
            } catch (IOException e) {
                Log.d("Kun", "Could not write file " + e.getMessage());
            }
        }
    }

    public static void flushAll() {
        flushDeviceAcc();
        flushDeviceGyro();
        flushResult();
        flushLocation();
        flushPhoneAcc();
    }

    public static void recordMetadata(String inString) {
        // Record metadata immediately because it's possible that user may switch to summary view
        // without disconnecting the device, so we need metadata to be "fresh" all the time
        try {
            BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                    sFileTimestamp + ".metadata"), false));
            wFile.write(inString);
            wFile.close();
        } catch (IOException e) {
            Log.d("Kun", "Could not write file " + e.getMessage());
        }
    }

    private static class DeviceAccRecordingTask extends AsyncTask<Void, Void, Void> {
        private int[] x;
        private int[] y;
        private int[] z;
        private long[] ts;
        private int size;

        DeviceAccRecordingTask(int[] x, int[] y, int[] z, long[] ts, int size) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.ts = ts;
            this.size = size;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            BufferedWriter wFile = null;
            try {
                wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                        sFileTimestamp + ".device_acc"),
                        true
                ));
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < size; i++) {
                    sb.append(x[i]);
                    sb.append(",");
                    sb.append(y[i]);
                    sb.append(",");
                    sb.append(z[i]);
                    sb.append(",");
                    sb.append(ts[i]);
                    sb.append("\n");
                }
                wFile.write(sb.toString());
                wFile.close();
            } catch (IOException e) {
                Log.d("Kun", "Could not write file " + e.getMessage());
            }
            return null;
        }
    }

    private static class DeviceGyroRecordingTask extends AsyncTask<Void, Void, Void> {
        private int[] x;
        private int[] y;
        private int[] z;
        private int size;

        DeviceGyroRecordingTask(int[] x, int[] y, int[] z, int size) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.size = size;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            BufferedWriter wFile = null;
            try {
                wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                        sFileTimestamp + ".device_gyro"), true
                ));
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < size; i++) {
                    sb.append(x[i]);
                    sb.append(",");
                    sb.append(y[i]);
                    sb.append(",");
                    sb.append(z[i]);
                    sb.append("\n");
                }
                wFile.write(sb.toString());
                wFile.close();
            } catch (IOException e) {
                Log.d("Kun", "Could not write file " + e.getMessage());
            }
            return null;
        }
    }

    private static class DeviceCntRecordingTask extends AsyncTask<Void, Void, Void> {
        private int[] pres1;
        private int[] pres2;
        private int[] pres3;
        private int[] pres4;
        private float[] gpsSpeed;
        private float[] gpsAlt;
        private long[] hr;
        private int size;

        DeviceCntRecordingTask(int[] pres1, int[] pres2, int[] pres3, int[] pres4, float[] gpsSpeedArray, float[] gpsAltArray, long[] hr, int size) {
            this.pres1 = pres1;
            this.pres2 = pres2;
            this.pres3 = pres3;
            this.pres4 = pres4;
            this.gpsSpeed = gpsSpeedArray;
            this.gpsAlt = gpsAltArray;
            this.hr = hr;
            this.size = size;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            BufferedWriter wFile = null;
            try {
                wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                        sFileTimestamp + ".device_pres"), true
                ));
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < size; i++) {
                    sb.append(pres1[i]);
                    sb.append(",");
                    sb.append(pres2[i]);
                    sb.append(",");
                    sb.append(pres3[i]);
                    sb.append(",");
                    sb.append(pres4[i]);
                    sb.append(",");
                    sb.append(gpsSpeed[i]);
                    sb.append(",");
                    sb.append(gpsAlt[i]);
                    sb.append(",");
                    sb.append(hr[i]);
                    sb.append("\n");
                }
                wFile.write(sb.toString());
                wFile.close();
            } catch (IOException e) {
                Log.d("Kun", "Could not write file " + e.getMessage());
            }
            return null;
        }
    }

    private static class ResultRecordingTask extends AsyncTask<Void, Void, Void> {
        private int[] walkCnts;
        private int[] runCnts;
        private float[] strideTimes;
        private float[] groundTimes;
        private float[] distanceYs;
        private float[] distanceZs;
        private float[] impactYs;
        private float[] impactZs;
        private float[] calories;
        private float[] fatigues;
        private long[] timestamps;
        private int size;

        ResultRecordingTask(int[] walkCnts, int[] runCnts, float[] strideTimes,
                            float[] groundTimes, float[] distanceYs, float[] distanceZs,
                            float[] impactYs, float[] impactZs, float[] calories,
                            float[] fatigues, long[] timestamps, int size) {
            this.walkCnts = walkCnts;
            this.runCnts = runCnts;
            this.strideTimes = strideTimes;
            this.groundTimes = groundTimes;
            this.distanceYs = distanceYs;
            this.distanceZs = distanceZs;
            this.impactYs = impactYs;
            this.impactZs = impactZs;
            this.calories = calories;
            this.fatigues = fatigues;
            this.timestamps = timestamps;
            this.size = size;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            BufferedWriter wFile = null;
            try {
                wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                        sFileTimestamp + ".result"),
                        true
                ));
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < size; i++) {
                    sb.append(walkCnts[i]);
                    sb.append(",");
                    sb.append(runCnts[i]);
                    sb.append(",");
                    sb.append(strideTimes[i]);
                    sb.append(",");
                    sb.append(groundTimes[i]);
                    sb.append(",");
                    sb.append(distanceYs[i]);
                    sb.append(",");
                    sb.append(distanceZs[i]);
                    sb.append(",");
                    sb.append(impactYs[i]);
                    sb.append(",");
                    sb.append(impactZs[i]);
                    sb.append(",");
                    sb.append(calories[i]);
                    sb.append(",");
                    sb.append(fatigues[i]);
                    sb.append(",");
                    sb.append(timestamps[i]);
                    sb.append("\n");
                }
                wFile.write(sb.toString());
                wFile.close();
            } catch (IOException e) {
                Log.d("Kun", "Could not write file " + e.getMessage());
            }
            return null;
        }
    }

    /**
     * GPS feedback is not that frequent, usually 2-5 seconds per number,
     * so we can afford doing very fine grain record in order to minimize data lost due to
     * whatever accident
     */
    private static class LocationRecordingTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                        sFileTimestamp + ".gps"),
                        true
                ));
                float[] sizes = {sLats.size(), sLngs.size(), sAlts.size(), sSpeedsGps.size(),
                        sLocTimestamp.size()};
                int minSize = (int) Util.min(sizes);

                for (int i = 0; i < minSize; i++) {
                    wFile.write(
                            sLats.get(i) + "," + sLngs.get(i) + "," + sAlts.get(i) + "," +
                            sSpeedsGps.get(i) + "," + sDistancesGps.get(i) + "," +
                            sSpeedsDevice.get(i) + "," + sDistancesDevice.get(i) + "," +
                            sLocTimestamp.get(i) + "\n");
                }
                wFile.close();
                sLats.clear();
                sLngs.clear();
                sAlts.clear();
                sSpeedsGps.clear();
                sDistancesGps.clear();
                sSpeedsDevice.clear();
                sDistancesDevice.clear();
                sLocTimestamp.clear();
            } catch (IOException e) {
                Log.d("Kun", "Could not write file " + e.getMessage());
            }
            return null;
        }
    }

    /**
     * GPS feedback is not that frequent, usually 2-5 seconds per number,
     * so we can afford doing very fine grain record in order to minimize data lost due to
     * whatever accident
     */
    private static class PhoneAccRecordingTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                BufferedWriter wFile = new BufferedWriter(new FileWriter(new File(FileUtil.ROOT_PATH +
                        sFileTimestamp + ".phone_acc"),
                        true
                ));
                float[] sizes = {sPhoneAccX.size(), sPhoneAccY.size(), sPhoneAccZ.size(),
                        sPhoneTimestamp.size()};
                int minSize = (int) Util.min(sizes);

                for (int i = 0; i < minSize; i++) {
                    wFile.write(
                            String.format("%.2f", sPhoneAccX.get(i)) + "," +
                                    String.format("%.2f", sPhoneAccY.get(i)) + "," +
                                    String.format("%.2f", sPhoneAccZ.get(i)) + "," +
                                    sPhoneTimestamp.get(i) + "\n"
                    );
                }
                wFile.close();
                sPhoneAccX.clear();
                sPhoneAccY.clear();
                sPhoneAccZ.clear();
                sPhoneTimestamp.clear();
            } catch (IOException e) {
                Log.d("Kun", "Could not write file " + e.getMessage());
            }
            return null;
        }
    }
}
